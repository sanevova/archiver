//
//  ARArchiverController.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/19/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARArchiverController : UIViewController
{
	UITextField *_source;
	UITextField *_destination;
}

@property (nonatomic, strong) IBOutlet UITextField *source;
@property (nonatomic, strong) IBOutlet UITextField *destination;

- (IBAction)encodeButtonClicked:(id)sender;
- (IBAction)decodeButtonClicked:(id)sender;

@end
