//
//  ARArchiverController.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/19/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import "ARArchiverController.h"
#import "ARArchiverManager.h"

@interface ARArchiverController ()

@end

@implementation ARArchiverController

@synthesize source = _source;
@synthesize destination = _destination;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)encodeButtonClicked:(id)sender
{
	if ([self filesAreOK])
	{
		[[ARArchiverManager sharedInstance] encodeFileNamed:self.source.text toDestination:self.destination.text];
		[[[[UIAlertView alloc] initWithTitle:@"Waddup!" message:@"Successfully Encoded!" delegate:nil cancelButtonTitle:@"I can be OK with that." otherButtonTitles:nil, nil] autorelease] show];
	}
}

- (IBAction)decodeButtonClicked:(id)sender
{
	if ([self filesAreOK])
	{
		[[ARArchiverManager sharedInstance] decodeFileNamed:self.source.text toDestination:self.destination.text];
		[[[[UIAlertView alloc] initWithTitle:@"Waddup!" message:@"Successfully Decoded!" delegate:nil cancelButtonTitle:@"That is wonderful." otherButtonTitles:nil, nil] autorelease] show];
	}
}

- (BOOL)existsFileNamed:(NSString *)targetFile isInputFile:(BOOL)isInputFile
{
	if (!isInputFile)
	{
		return YES;
		//	as it is willing to be created
	}
	NSString *path = SimulatorDownloadsPath;
    NSError *error = nil;
    NSFileManager *manager = [NSFileManager defaultManager];
    NSArray *files = [manager contentsOfDirectoryAtPath:path error:&error];
	
	BOOL found = NO;
    for (NSString *fileName in files)
    {
		if ([targetFile isEqualToString:fileName])
		{
			found = YES;
		}
    };
	NSString *fileType = (isInputFile ? @"source file!" : @"destination file!");
	if (!found)
	{
		[[[[UIAlertView alloc] initWithTitle:@"Hang on a second.." message:[@"There is no such " stringByAppendingString:fileType] delegate:nil cancelButtonTitle:@"OK, I'm sorry..." otherButtonTitles:nil, nil] autorelease] show];
	}
	return found;
}

- (BOOL)filesAreOK
{
	return [self existsFileNamed:self.source.text isInputFile:YES] && [self existsFileNamed:self.destination.text isInputFile:NO];
}

@end
