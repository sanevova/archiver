//
//  AppDelegate.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "Foundation/Foundation.h"
#import "UIKit/UIKit.h"
#import "AppDelegate.h"
#import "ARTree.h"
#import "ARInputFile.h"
#import "AROutputFile.h"
#import "ARHuffmanEncoder.h"
#import "ARHuffmanDecoder.h"
#import "ARArchivedFile.h"
#import "ARArchiverController.h"

#define CurrentUserName @"vladimir.mishatkin"
#define SimulatorVersion @"6.0"
#define SimulatorDownloadsPath [[[[@"/Users/" stringByAppendingString:CurrentUserName] stringByAppendingString:@"/Library/Application Support/iPhone Simulator/"] stringByAppendingString:SimulatorVersion] stringByAppendingString:@"/Media/Downloads/"]
#define ARFileInDownloadsFolder(_fileName_) ([SimulatorDownloadsPath stringByAppendingString:_fileName_])

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
	
	self.window.rootViewController = [[[UIViewController alloc] initWithNibName:nil bundle:nil] autorelease];
//	UIViewController *rootController = self.window.rootViewController;
//	rootController.view.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
	
	UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Lobster.jpg"]];
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	ARArchiverController *archiver = [[[ARArchiverController alloc] initWithNibName:@"ARArchiverController" bundle:[NSBundle mainBundle]] autorelease];
	[archiver view];
	archiver.view.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
	[archiver.view addSubview:logo];
	logo.center = archiver.view.center;
	[archiver.view sendSubviewToBack:logo];
	self.window.rootViewController = archiver;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//	NSString *path = SimulatorDownloadsPath;
//    NSError *error = nil;
//    NSFileManager *manager = [NSFileManager defaultManager];
//    NSArray *files = [manager contentsOfDirectoryAtPath:path error:&error];
//
//    for (NSString *fileName in files)
//    {
//		NSLog(@"%@", fileName);
//    };

//	ARArchivedFile *abcFile = [[[ARArchivedFile alloc] initWithFileNamed:ARFileInDownloadsFolder(@"ABC.txt")] autorelease];
	
//	ARInputFile *abcFile = [[[ARInputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(@"LoremIpsum.txt")] autorelease];
//	AROutputFile *destination = [[[AROutputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(@"loremm.bin")] autorelease];
////
//	ARHuffmanEncoder *encoder = [[[ARHuffmanEncoder alloc] initWithInputSource:abcFile] autorelease];
//	[encoder encodeToDestination:destination];

//	ARInputFile *fileToDecode = [[[ARInputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(@"loremm.bin")] autorelease];
//	ARHuffmanDecoder *decoder = [[[ARHuffmanDecoder alloc] initWithInputSource:fileToDecode] autorelease];
//	AROutputFile *decodeDestination = [[[AROutputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(@"loremm.txt")] autorelease];
//	[decoder decodeToDestination:decodeDestination];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	NSLog(@"will terminate");
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
