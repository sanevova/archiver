//
//  ARConstants.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

extern NSUInteger const kNumberOfPossibleCharacters;
extern NSUInteger const kByteSizeInBits;
extern NSUInteger const kBitMaskSize;