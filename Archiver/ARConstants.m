//
//  ARConstants.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARConstants.h"

NSUInteger const kNumberOfPossibleCharacters = 256;
NSUInteger const kByteSizeInBits = 8;
NSUInteger const kBitMaskSize = kNumberOfPossibleCharacters / kByteSizeInBits;	//	in bytes