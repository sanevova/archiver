//
//  main.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool
	{
	    return UIApplicationMain(argc, argv, nil, @"AppDelegate");
	}
}
