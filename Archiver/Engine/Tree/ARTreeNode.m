//
//  ARTreeNode.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARTreeNode.h"

@interface ARTreeNode ()
{

}


- (BOOL)isEqual:(id)object;
- (NSUInteger)hash;

@end

@implementation ARTreeNode



@synthesize frequency = _frequency;
@synthesize character = _character;
@synthesize left = _left;
@synthesize right = _right;

+ (id)nodeWithCharacter:(char)key frequency:(int)value
{
	ARTreeNode *node = [[[[self class] alloc] init] autorelease];
	if (nil != node)
	{
		node.character = [NSNumber numberWithChar:key];
		node.frequency = [NSNumber numberWithInt:value];
		node.left = nil;
		node.right = nil;
	}
	return node;
}

+ (id)nodeWithNodesLeft:(id)leftNode right:(id)rightNode
{
	ARTreeNode *node = [[[[self class] alloc] init] autorelease];
	if (nil != self)
	{
		node.left = leftNode;
		node.right = rightNode;
		node.frequency  = [NSNumber numberWithInt:[[leftNode frequency] intValue] + [[rightNode frequency] intValue]];
		//	stores nothing
		node.character = '\0';
	}
	return node;
}

- (void)dealloc
{
	self.frequency = nil;
	self.character = nil;
	self.left = nil;
	self.right = nil;
	[super dealloc];
}

- (BOOL)isLeaf
{
	return (nil == self.left && nil == self.right);
}

- (BOOL)isEqual:(id)object
{
	if ([object respondsToSelector:@selector(frequency)])
		return [self.frequency isEqual:[object frequency]];
	return NO;
}

- (NSUInteger)hash
{
	return [self.frequency hash];
}

- (NSString *)description
{
	return [[NSString stringWithFormat:@"\t%c = %d", [self.character charValue], [self.frequency intValue]] stringByAppendingFormat:@""];//@"\tleft -> %@, \tright -> %@\n", self.left, self.right];
}

@end
