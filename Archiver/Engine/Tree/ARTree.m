//
//  ARTree.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARTree.h"
#import "ARTreeNode.h"
#import "ARConstants.h"

@interface ARTree ()
{
	
}

@property (nonatomic, strong) NSMutableDictionary *codes;

@end

@implementation ARTree

@synthesize root = _root;
@synthesize codes = _codes;

- (id)initWithDictionary:(NSDictionary *)frequenciesDictionary
{
	self = [super init];
	if (nil != self)
	{
		NSMutableArray *nodes = [NSMutableArray arrayWithCapacity:kNumberOfPossibleCharacters];
		for (NSString *key in frequenciesDictionary)
		{
			int frequency = [[frequenciesDictionary valueForKey:key] intValue];
			char nodeCharacter = ([key length] ? [key characterAtIndex:0] : '\0');
			ARTreeNode *newNode = [ARTreeNode nodeWithCharacter:nodeCharacter frequency:frequency];
			[nodes addObject:newNode];
		}
		[nodes sortUsingComparator:^(id item1, id item2)
										{
											return [[item2 frequency] compare:[item1 frequency]];
										}];
		int count = [nodes count];
		for (int last = count - 1; last >= 1; --last)
		{
			ARTreeNode *leftNode = [nodes objectAtIndex:last - 1];
			ARTreeNode *rightNode = [nodes objectAtIndex:last];
			ARTreeNode *united = [[[ARTreeNode nodeWithNodesLeft:leftNode right:rightNode] retain] autorelease];
			
			//	find a right place to insert
			BOOL found = NO;
			int indexToInsert = last - 1;
			for (int position = last - 2; !found && position >= 0; --position)
			{
				ARTreeNode *currentNode = [nodes objectAtIndex:position];
				if ([currentNode.frequency intValue] >= [united.frequency intValue])
				{
					found = YES;
					indexToInsert = position + 1;
				}
			}
			if (!found)
			{
				//	the most frequent character goes to the top
				indexToInsert = 0;
			}

			//	shifting all objects by 1 position right
			for (int i = last - 2; i >= indexToInsert; --i)
			{
				id object = [nodes objectAtIndex:i];
				[nodes setObject:object atIndexedSubscript:i + 1];
			}
			[nodes setObject:united atIndexedSubscript:indexToInsert];
		}
		self.root = [nodes objectAtIndex:0];
	}
	return self;
}

- (NSDictionary *)codesForAllCharacters
{
	if (!self.codes)
	{
		[self encodeValuesFromNode:self.root withPath:@""];
	}
	return [NSDictionary dictionaryWithDictionary:self.codes];
}

//+ (Class)treeNodeClass
//{
//	return [ARTreeNode class];
//}
//
//+ (BOOL)resolveInstanceMethod:(SEL)sel
//{
//	return YES;
//	if ([super resolveInstanceMethod:sel])
//		return YES;
//	return [[self treeNodeClass] respondsToSelector:sel];
//}
//
//- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
//{
//    return [ARTreeNode instanceMethodSignatureForSelector:aSelector];
//}
//
//- (void)forwardInvocation:(NSInvocation *)anInvocation
//{
////	[super forwardInvocation:anInvocation];
//	SEL selector = [anInvocation selector];
////	[self.root performSelector:selector];
//	[self.root performSelector:selector withObject:@""];
//}
- (void)encodeValuesFromNode:(ARTreeNode *)node withPath:(NSString *)path
{
	if ([path isEqualToString:@""])
	{
		self.codes = nil;
		self.codes = [[NSMutableDictionary alloc] init];
	}
	
	if ([node isLeaf])
	{
		[self.codes setValue:path forKey:[NSString stringWithFormat:@"%c", [node.character charValue]]];
	}
	else
	{
		[self encodeValuesFromNode:node.left withPath:[NSString stringWithFormat:@"%@0", path]];
		[self encodeValuesFromNode:node.right withPath:[NSString stringWithFormat:@"%@1", path]];
	}
}

- (void)dealloc
{
	self.root = nil;
	[super dealloc];
}

- (NSUInteger)tailLength
{
	return kByteSizeInBits - [self inorderTreeWalkFromNode:self.root] % kByteSizeInBits;
}

- (NSUInteger)inorderTreeWalkFromNode:(ARTreeNode *)node
{
	static NSUInteger currentCodeLength = 0;
	if ([node isLeaf])
	{
		return currentCodeLength * [node.frequency intValue];
	}
	++currentCodeLength;
	NSUInteger leftTail = [self inorderTreeWalkFromNode:node.left];
	NSUInteger rightTail = [self inorderTreeWalkFromNode:node.right];
	--currentCodeLength;
	return leftTail + rightTail;
}
@end
