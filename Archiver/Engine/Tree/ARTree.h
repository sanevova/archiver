//
//  ARTree.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARTreeNode;

@interface ARTree : NSObject
{
@private
	ARTreeNode *_root;
	NSMutableDictionary *_codes;
}

@property (nonatomic, strong) ARTreeNode *root;

- (id)initWithDictionary:(NSDictionary *)frequenciesDictionary;
- (NSDictionary *)codesForAllCharacters;
- (NSUInteger)tailLength;

@end
