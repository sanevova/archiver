//
//  ARTreeNode.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/14/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARTreeNode : NSObject
{
@private
	NSNumber *_frequency;
	NSNumber *_character;
	ARTreeNode *_left;
	ARTreeNode *_right;
}

@property (nonatomic, assign) NSNumber *frequency;
@property (nonatomic, assign) NSNumber *character;
@property (nonatomic, strong) ARTreeNode *left;
@property (nonatomic, strong) ARTreeNode *right;

//	Factory mehtods
+ (id)nodeWithCharacter:(char)key frequency:(int)value;
+ (id)nodeWithNodesLeft:(id)leftNode right:(id)rightNode;

//- (id)initWithDictionary:(NSDictionary *)dictionary;
//- (NSDictionary *)encodeValues;
- (BOOL)isLeaf;

@end
