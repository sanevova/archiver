//
//  NSString+ARStringToBinaryExtension.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import "NSString+ARStringToBinaryExtension.h"

@implementation NSString (ARStringToBinaryExtension)

- (unsigned char)parseToBinary
{
	unsigned char retValue = 0;
	for (int i = 0 ; i < 8 && i < [self length]; ++i)
	{
		BOOL nextBit = ([self characterAtIndex:i] != '0');
		retValue = (retValue << 1) + nextBit;
	}
	return retValue;
}

@end