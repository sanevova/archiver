//
//  ARHuffmanEncoder.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARHuffmanEncoder.h"
#import "ARTree.h"
#import "ARConstants.h"
#import "NSString+ARStringToBinaryExtension.h"

@interface ARHuffmanEncoder ()
{
@private
	id <ARInputSource> _inputSource;
	NSString *_unstagedCode;
	ARTree *_huffmanTree;
	NSDictionary *_frequencies;
}

@property (nonatomic, strong) id<ARInputSource> inputSource;
@property (nonatomic, strong) NSString *unstagedCode;
@property (nonatomic, strong) ARTree *huffmanTree;
@property (nonatomic, strong) NSDictionary *frequencies;

@end


@implementation ARHuffmanEncoder

@synthesize inputSource = _inputSource;
@synthesize unstagedCode = _unstagedCode;
@synthesize huffmanTree = _huffmanTree;
@synthesize frequencies = _frequencies;

- (id)initWithInputSource:(id<ARInputSource>)source
{
	self = [super init];
	if (nil != self)
	{
		self.inputSource = source;
		self.unstagedCode = @"";
		self.huffmanTree = nil;
		[self setupCharactersFrequencies];
	}
	return self;
}

- (void)dealloc
{
	self.inputSource = nil;
	self.unstagedCode = nil;
	self.huffmanTree = nil;
	self.frequencies = nil;
	[super dealloc];
}

#pragma mark - Second input check
- (void)encodeToDestination:(id<AROutputSource>)destination
{
//#warning testing!
//	NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:100];
//	for (int i = 2; i >= 0 ; --i)
//	{
//		int lol = 2 * i + 5;
//		[dictionary setValue:[NSNumber numberWithInt:lol] forKey:[NSString stringWithFormat:@"%c", (char)(65 + i)]];
//	}
//	NSDictionary *frequencies = dictionary;
////	--------------------
//	
	NSDictionary *frequencies = self.frequencies;
	self.huffmanTree = [[[ARTree alloc] initWithDictionary:frequencies] autorelease];
	NSDictionary *codes = [self codesForAllCharacters];
	NSLog(@"Codes are: %@", codes);
	NSLog(@"Tail is %d long.", [self tailLength]);
	[self printHeaderToDestination:destination];
	
	while ([self.inputSource hasNext])
	{
//		NSString *key = @"More than 1 character length string.";
//		@try
//		{
		NSString *key = [NSString stringWithFormat:@"%c", [self.inputSource next]];
//		}
//		@catch (NSException *exception)
//		{
//			NSLog(@"Exception caught: %@", exception);
//		}
		NSString *code = [codes valueForKey:key];
		self.unstagedCode = [self.unstagedCode stringByAppendingString:code];
		while ([self.unstagedCode length] >= kByteSizeInBits)
		{
			[self printUnstagedPackage:destination];
		}
	}
		
	//	printing remained part with appended zeros
	NSString *zeroString = [NSString stringWithFormat:@"%c", '0'];
	while ([self.unstagedCode length] < kByteSizeInBits)
	{
		self.unstagedCode = [self.unstagedCode stringByAppendingString:zeroString];
	}
	[self printUnstagedPackage:destination];
	
}

- (void)printUnstagedPackage:(id<AROutputSource>)destination
{
	char valueToPrint = [self.unstagedCode parseToBinary];
	[destination printPackage:valueToPrint];
	if (kByteSizeInBits == [self.unstagedCode length])
	{
		self.unstagedCode = @"";
	}
	else
	{
		self.unstagedCode = [self.unstagedCode substringFromIndex:kByteSizeInBits];
	}
}

#pragma mark - First input check
- (void)setupCharactersFrequencies
{
	if (!self.frequencies)
	{
		int frequencies[kNumberOfPossibleCharacters];
		for (int i = 0; i < kNumberOfPossibleCharacters; ++i)
		{
			frequencies[i] = 0;
		}
		
		while ([self.inputSource hasNext])
		{
//			@try
//			{
				int correspondingIndex = [self.inputSource next];
//				if (correspondingIndex >= 0)
//				{
					++frequencies[correspondingIndex];
//				}
//			}
//			@catch (NSException *exception)
//			{
//				NSLog(@"Exception caught: %@", exception);
//			}
		}
		
		for (int i = 0; i < 256; ++i)
		{
			if (frequencies[i] > 0)
			{
				NSLog(@"%c - %d", i, frequencies[i]);
			}
		}
		NSMutableDictionary *frequenciesDictionary = [[[NSMutableDictionary alloc] init] autorelease];
		for (int i = 0; i < kNumberOfPossibleCharacters; ++i)
		{
			if (frequencies[i] > 0)
			{
				NSString *key = [NSString stringWithFormat:@"%c", (char)i];
				NSNumber *value = [NSNumber numberWithInt:frequencies[i]];
				[frequenciesDictionary setValue:value forKey:key];
			}
		}
		self.frequencies = [NSDictionary dictionaryWithDictionary:frequenciesDictionary];
		[self.inputSource reset];
	}
}

- (NSUInteger)tailLength
{
	return [self.huffmanTree tailLength];

}

- (NSDictionary *)codesForAllCharacters
{
	return [self.huffmanTree codesForAllCharacters];
}

- (void)printHeaderToDestination:(id<AROutputSource>)destination
{
	NSDictionary *codes = [self codesForAllCharacters];
//	Setting bit mask
	unsigned char bitMask[kBitMaskSize];
	char existingCharacters[kNumberOfPossibleCharacters];
	int numberOfExistingCharacters = 0;
	for (int i = 0; i < kBitMaskSize; ++i)
	{
		bitMask[i] = 0;
	}
	for (char ch = 0; ch < kNumberOfPossibleCharacters; ++ch)
	{
		NSString *key = [NSString stringWithFormat:@"%c", ch];
		if (nil != [codes objectForKey:key])
		{
			existingCharacters[ numberOfExistingCharacters++ ] = ch;
			int index = ch / kByteSizeInBits;
			int shift = (ch % kByteSizeInBits);
			unsigned char newBit = (1 << (kByteSizeInBits - 1 - shift));
			bitMask[index] |= newBit;
//			NSLog(@"bitMask[index] = %d", bitMask[index]);
		}
	}
//	Printing Header
	//	printing bit mask
	for (int i = 0; i < kBitMaskSize; ++i)
	{
//		NSLog(@"%d", bitMask[i]);
		[destination printPackage:bitMask[i]];
	}
	//	printing frequencies
	for (int i = 0; i < numberOfExistingCharacters; ++i)
	{
		NSString *key = [NSString stringWithFormat:@"%c", existingCharacters[i]];
		int frequency = [[self.frequencies valueForKey:key] intValue];
		//	byte printing of an int
		for (int part = 0; part < 4; ++part)
		{
			unsigned char package;
			if (4 - 1 - part != 0)
			{
				package = frequency / (1 << ((4 - 1 - part) * kByteSizeInBits)) % 256;
			}
			else
			{
				package = frequency % 256;
				
			}
			[destination printPackage:package];
		}
	}
	//	printing tail length
	[destination printPackage:(char)[self tailLength]];
}

//- (char)nextCharacter
//{
//	@try
//	{
//	}
//	@catch (NSException *exception)
//	{
//		NSLog(@"Exception caught: %@", exception);
//	}
//}

@end
