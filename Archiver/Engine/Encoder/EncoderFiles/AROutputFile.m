//
//  AROutputFile.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/27/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "AROutputFile.h"

@interface AROutputFile ()

@end

@implementation AROutputFile

- (id)initWithFileNamed:(NSString *)fileName
{
	[super initWithFileNamed:fileName openFormat:"wb"];
	if (nil != self)
	{

	}
	return self;
}

- (void)printPackage:(unsigned char)package
{
	static int total = 0;
	NSLog(@"Write %d: %d", ++total, package);
	const void *mem = &package;
	fwrite(mem, sizeof(char), 1, self.file);
}


@end
