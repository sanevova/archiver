//
//  ARInputFile.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARFile.h"
#import "ARInputSource.h"

@interface ARInputFile : ARFile <ARInputSource>

- (id)initWithFileNamed:(NSString *)fileName;

@end
