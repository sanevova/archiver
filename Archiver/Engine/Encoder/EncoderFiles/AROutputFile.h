//
//  AROutputFile.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/27/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARFile.h"
#import "AROutputSource.h"

@interface AROutputFile : ARFile <AROutputSource>

- (id)initWithFileNamed:(NSString *)fileName;

@end
