//
//  ARInputFile.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARInputFile.h"

@interface ARInputFile ()

@end

@implementation ARInputFile

- (id)initWithFileNamed:(NSString *)fileName
{
	[super initWithFileNamed:fileName openFormat:"rb"];
	if (nil != self)
	{
		
	}
	return self;
}

- (void)reset
{
	fseek(self.file, 0, 0);
}

- (unsigned char)next
{
	if (![self hasNext])
	{
		NSException *eofException = [[NSException alloc] initWithName:@"EOFException" reason:@"File is over and does not haveNext. Access denied." userInfo:nil];
		[eofException raise];
	}
	unsigned char nextChar;
	fscanf(self.file, "%c", &nextChar);
	return nextChar;
}

- (BOOL)hasNext
{
	return !feof(self.file);
}

@end
