//
//  ARHuffmanEncoder.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARInputSource.h"
#import "AROutputSource.h"

@interface ARHuffmanEncoder : NSObject

- (id)initWithInputSource:(id<ARInputSource>)source;
- (void)encodeToDestination:(id<AROutputSource>)destination;

@end
