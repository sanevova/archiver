//
//  ARArchiverManager.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/19/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import "ARArchiverManager.h"
#import "ARHuffmanEncoder.h"
#import "ARHuffmanDecoder.h"
#import "ARInputFile.h"
#import "AROutputFile.h"

@implementation ARArchiverManager

static ARArchiverManager *aSharedInstance = nil;

+ (ARArchiverManager *)sharedInstance
{
	@synchronized(aSharedInstance)
	{
		if (nil == aSharedInstance)
		{
			aSharedInstance = [[super allocWithZone:NSDefaultMallocZone()] init];
		}
		return aSharedInstance;
	}
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedInstance];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (NSUInteger)retainCount
{
	return NSUIntegerMax;
}

- (id)retain
{
	return self;
}

- (oneway void)release
{
	//	no way void release lol
}

- (id)autorelease
{
	return self;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		//	Custom Initialization
	}
	return self;
}

- (void)dealloc
{
	[super dealloc];
}

- (void)encodeFileNamed:(NSString *)sourceFileName toDestination:(NSString *)destinationFileName
{
	ARInputFile *sourceFile = [[[ARInputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(sourceFileName)] autorelease];
	AROutputFile *destination = [[[AROutputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(destinationFileName)] autorelease];
	ARHuffmanEncoder *encoder = [[[ARHuffmanEncoder alloc] initWithInputSource:sourceFile] autorelease];
	[encoder encodeToDestination:destination];
}

- (void)decodeFileNamed:(NSString *)sourceFileName toDestination:(NSString *)destinationFileName
{
	ARInputFile *fileToDecode = [[[ARInputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(sourceFileName)] autorelease];
	ARHuffmanDecoder *decoder = [[[ARHuffmanDecoder alloc] initWithInputSource:fileToDecode] autorelease];
	AROutputFile *decodingDestination = [[[AROutputFile alloc] initWithFileNamed:ARFileInDownloadsFolder(destinationFileName)] autorelease];
	[decoder decodeToDestination:decodingDestination];
}

@end
