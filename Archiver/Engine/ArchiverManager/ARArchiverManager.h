//
//  ARArchiverManager.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/19/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#define CurrentUserName @"vladimir.mishatkin"
#define SimulatorVersion @"6.0"
#define SimulatorDownloadsPath [[[[@"/Users/" stringByAppendingString:CurrentUserName] stringByAppendingString:@"/Library/Application Support/iPhone Simulator/"] stringByAppendingString:SimulatorVersion] stringByAppendingString:@"/Media/Downloads/"]
#define ARFileInDownloadsFolder(_fileName_) ([SimulatorDownloadsPath stringByAppendingString:_fileName_])

#import <Foundation/Foundation.h>

@interface ARArchiverManager : NSObject

+ (ARArchiverManager *)sharedInstance;

- (void)encodeFileNamed:(NSString *)sourceFileName toDestination:(NSString *)destinationFileName;
- (void)decodeFileNamed:(NSString *)sourceFileName toDestination:(NSString *)destinationFileName;

@end
