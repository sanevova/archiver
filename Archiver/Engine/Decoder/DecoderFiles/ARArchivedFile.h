//
//  ARArchivedFile.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARFile.h"
#import "ARInputSource.h"

//	deprecated

@interface ARArchivedFile : ARFile <ARInputSource>

- (id)initWithFileNamed:(NSString *)fileName;

@end
