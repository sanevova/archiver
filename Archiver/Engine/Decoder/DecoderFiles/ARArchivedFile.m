//
//  ARArchivedFile.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import "ARArchivedFile.h"
#import "ARConstants.h"

@interface ARArchivedFile ()

@end

@implementation ARArchivedFile

- (id)initWithFileNamed:(NSString *)fileName
{
	[super initWithFileNamed:fileName openFormat:"rb"];
	if (nil != self)
	{
		
	}
	return self;
}

- (void)reset
{
	fseek(self.file, 0, 0);
}

- (unsigned char)next
{
	static int total = 0;
	unsigned char nextByte = 0;
	if (![self hasNext])
	{
		NSException *eofException = [[NSException alloc] initWithName:@"EOFException" reason:@"File is over and does not haveNext. Access denied." userInfo:nil];
		[eofException raise];
	}
	fread(&nextByte, 1, 1, self.file);
	NSLog(@"Read %d: %d", ++total, nextByte);
	return nextByte;
}

- (BOOL)hasNext
{
	return !feof(self.file);
}

@end
