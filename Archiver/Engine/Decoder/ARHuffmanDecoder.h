//
//  ARHuffmanDecoder.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/30/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARInputSource.h"
#import "AROutputSource.h"

@interface ARHuffmanDecoder : NSObject

- (id)initWithInputSource:(id<ARInputSource>)source;
- (void)decodeToDestination:(id<AROutputSource>)destination;

@end
