//
//  ARHuffmanDecoder.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/30/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import "ARHuffmanDecoder.h"
#import "ARTree.h"
#import "ARTreeNode.h"
#import "ARConstants.h"
#import "NSString+ARStringToBinaryExtension.h"

@interface ARHuffmanDecoder ()
{
@private
	id <ARInputSource> _inputSource;
	ARTree *_huffmanTree;
	NSUInteger _tailLength;
	unsigned char _package;
	NSString *_code;
}

@property (nonatomic, strong) id<ARInputSource> inputSource;
@property (nonatomic, strong) ARTree *huffmanTree;
@property (nonatomic) NSUInteger tailLength;
@property (nonatomic) unsigned char package;
@property (nonatomic, strong) NSString *code;

@end

@implementation ARHuffmanDecoder

@synthesize inputSource = _inputSource;
@synthesize huffmanTree = _huffmanTree;
@synthesize tailLength = _tailLength;
@synthesize package = _package;
@synthesize code = _code;

- (id)initWithInputSource:(id<ARInputSource>)source
{
	self = [super init];
	if (nil != self)
	{
		self.inputSource = source;
		[self regrowTree];
		self.tailLength = [self nextByte];
		self.package = 0;
		self.code = @"";
	}
	return self;
}

- (void)dealloc
{
	self.inputSource = nil;
	self.huffmanTree = nil;
	self.code = nil;
	[super dealloc];
}

- (void)decodeToDestination:(id<AROutputSource>)destination
{
	while ([self.inputSource hasNext])
	{
		unsigned char package = [self nextByte];
		int length = kByteSizeInBits;
		if (![self.inputSource hasNext])
		{
			length -= self.tailLength;
		}
		NSString *remainingPackage = @"";
		for (int i = 0; i < length; ++i)
		{
			BOOL nextBit = ((package >> (kByteSizeInBits - 1 - i)) & 1);
			remainingPackage = [remainingPackage stringByAppendingFormat:@"%d", nextBit];
		}
		self.code = [self.code stringByAppendingString:remainingPackage];
		BOOL containsSomeCode = YES;
		while (containsSomeCode)
		{
			ARTreeNode *currentNode = self.huffmanTree.root;
			int i;
			for (i = 0; i < [self.code length] - kByteSizeInBits && ![currentNode isLeaf]; ++i)
			{
				if ([self.code characterAtIndex:i] - '0')
				{
					currentNode = currentNode.right;
				}
				else
				{
					currentNode = currentNode.left;
				}
			}
			if ([currentNode isLeaf])
			{
				unsigned char currentCharacter = [[currentNode character] charValue];
				[destination printPackage:currentCharacter];
				if (i < [self.code length])
				{
					self.code = [self.code substringFromIndex:i];
				}
				else
				{
					self.code = @"";
				}
			}
			else
			{
				containsSomeCode = NO;
			}
		}
	}

}

- (void)regrowTree
{
	self.huffmanTree = nil;
	unsigned char bitMask[kBitMaskSize];
	//	reading Mask
	for (int i = 0; i < kBitMaskSize; ++i)
	{
		bitMask[i] = [self nextByte];
	}
	
	//reading frequencies
	NSMutableDictionary *frequencies = [[[NSMutableDictionary alloc] init] autorelease];
	for (char ch = 0; ch < kNumberOfPossibleCharacters; ++ch)
	{
		int index = ch / kByteSizeInBits;
		int shift = (ch % kByteSizeInBits);
		unsigned char bit = (1 << (kByteSizeInBits - 1 - shift));
		if (bitMask[index] & bit)
		{
			// character occures in file
			int frequency = 0;
			//	byte printing of an int
			for (int part = 0 ; part < 4; ++part)
			{
				unsigned char package = [self nextByte];
				frequency |= (package << ((4 - 1 - part) * kByteSizeInBits));
			}
			NSString *key = [NSString stringWithFormat:@"%c", ch];
			NSNumber *value = [NSNumber numberWithInt:frequency];
			[frequencies setValue:value forKey:key];
		}
	}
	NSDictionary *frequenciesDiectionary = [NSDictionary dictionaryWithDictionary:frequencies];
	self.huffmanTree = [[[ARTree alloc] initWithDictionary:frequenciesDiectionary] autorelease];
}

- (unsigned char)nextByte
{
	unsigned char retValue = 0;
	@try
	{
		retValue = [self.inputSource next];
	}
	@catch (NSException *exception)
	{
		NSLog(@"Exception caught: %@", exception);
	}
	return retValue;
}

@end
