//
//  ARFile.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARFile : NSObject
{
@private
	FILE *_file;
}

@property (nonatomic) FILE *file;

- (id)initWithFileNamed:(NSString *)fileName openFormat:(const char *)openFormat;

@end
