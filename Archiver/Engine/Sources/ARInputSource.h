//
//  ARInputSource.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ARInputSource <NSObject>

- (void)reset;
- (BOOL)hasNext;
- (unsigned char)next;

@end
