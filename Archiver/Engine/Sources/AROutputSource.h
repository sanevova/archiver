//
//  AROutputSource.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 12/25/12.
//  Copyright (c) 2012 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AROutputSource <NSObject>

- (void)printPackage:(unsigned char)package;	// prints a single byte of data
//(NSString *)aString;

@end
