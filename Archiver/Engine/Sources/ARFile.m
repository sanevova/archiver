//
//  ARFile.m
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import "ARFile.h"

@interface ARFile ()

@end

@implementation ARFile

@synthesize file = _file;

- (id)initWithFileNamed:(NSString *)fileName openFormat:(const char *)openFormat
{
	[super init];
	if (nil != self)
	{
		char *fileNameCString = (char *)malloc([fileName length] + 1);
		int length = [fileName length];
		for (int i = 0; i < length; ++i)
		{
			fileNameCString[i] = [fileName characterAtIndex:i];
		}
		fileNameCString[length] = '\0';
		self.file = fopen(fileNameCString, openFormat);
		if (!self.file)
		{
			NSLog(@"No such file: %@", fileName);
		}
		free(fileNameCString);
	}
	return self;
}

- (void)dealloc
{
	fclose(self.file);
	self.file = nil;
	[super dealloc];
}

@end
