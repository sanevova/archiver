//
//  NSString+ARStringToBinaryExtension.h
//  Archiver
//
//  Created by Vladimir Mishatkin on 1/12/13.
//  Copyright (c) 2013 LobsterSolutuins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ARStringToBinaryExtension)

- (unsigned char)parseToBinary;

@end